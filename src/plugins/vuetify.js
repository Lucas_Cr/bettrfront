// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify)

const opts = {
      theme: {
        options: {
          customProperties: true
        },
        themes: {
          dark: {
            primary: '#1f1e1e',
            secondary: '#fffce1',
            neutral: '#272727',
            accent1: '#ffbb33',
            accent2: '#ffbb33',
            error: '#ff4444',
            info: '#33b5e5',
            success: '#00C851',
            warning: '#FF652F',


            //original
          //  primary: '#538f80',
          //   secondary: '#fffce1',
          //   neutral: '#f9f8f7',
          //   accent1: 'ff6a00',
          //   accent2: '#2bb4b0',
          //   error: '#ff4444',
          //   info: '#33b5e5',
          //   success: '#00C851',
          //   warning: '#ffbb33'
          },
        },dark: true
      },
      
    // iconfont: 'md' // 'md' || 'mdi' || 'fa' || 'fa4'
}

export default new Vuetify(opts)