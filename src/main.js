import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import moment from 'moment';

Vue.use(VueRouter)
Vue.use(Vuex);
Vue.use(VueAxios, axios)


Vue.config.productionTip = false

// Global axios configuration

axios.defaults.baseURL = 'https://quiet-fjord-54293.herokuapp.com//api/v1';
// axios.defaults.baseURL = 'http://localhost:3000//api/v1';
axios.interceptors.request.use(function (config) {
  const uid = localStorage.getItem('uid');
  const access = localStorage.getItem('access-token');
  const client = localStorage.getItem('client');
  config.headers.common["uid"] =  uid ? uid : '';
  config.headers.common["access-token"] =  access ? access : '';
  config.headers.common["client"] =  client ? client : '';

  config.headers.common["Accept"] = '*/*';
  config.headers.common["Access-Control-Allow-Origin"] = '*';
  config.headers.common["Content-Type"] =  "application/json";
  
  return config;
});

// Global date configuration

Vue.filter('formatDate', function(value) {
  if (value) {
      return moment(String(value)).format('MM/DD/YYYY')
  }
});

// Static pages
// import About from "./components/static_pages/about.vue";
// import Contact from "./components/static_pages/contact.vue";
// import Help from "./components/static_pages/help.vue";
import Home from "./components/static_pages/home.vue";
import Options from "./components/static_pages/options.vue";
// import ActivationPage from "./components/static_pages/activationPage.vue";
// import AwaitingActivation from "./components/static_pages/awaitingActivation.vue";

// Password_resets
import New_password_reset from "./components/password_resets/new.vue"
import Edit_password_reset from "./components/password_resets/edit.vue"
// Sessions
import New_session from "./components/sessions/new.vue"
// Users
import New_user from "./components/users/new.vue"
import Show_user from "./components/users/show.vue"
import Edit_user from "./components/users/edit.vue"
// import User_index from "./components/users/index.vue"
import Find_users from "./components/users/findUsers.vue"
import User_wrapper from "./components/users/showWrapper.vue"

// Bets
import Find_bets from "./components/bets/findBets.vue"
import Bet_wrapper from "./components/bets/showWrapper.vue"
import Builder from "./components/bets/builder.vue"

// Vuex
import { store } from './store'
// Example for lazy-loading component
// {
//   path: "/about",
//   name: "About",
//   component: () =>
//     import(/* webpackChunkName: "about" */ "../views/About.vue"),
// }

const router = new VueRouter({
  routes:
  [

        // { path: '/about',         component: About },
        // { path: '/contact',       component: Contact },
        // { path: '/help',          component: Help },
        { path: '/home',          component: Home },
        { path: '/newpassword',   component: New_password_reset },
        { path: '/editpassword',  component: Edit_password_reset},
        { path: '/login',         component: New_session },
        { path: '/user',          component: Show_user },
        { path: '/users',         component: Find_users },
        { path: '/user/show/',     component: User_wrapper },
        { path: '/signup',        component: New_user},
        { path: '/user/edit',   component: Edit_user},
        { path: '/bets',         component: Find_bets },
        { path: '/bet/show/',     component: Bet_wrapper },
        { path: '/bet/new/',     component: Builder },
        { path: '/options',     component: Options },
        // { path: '/activationPage',component: ActivationPage},
        // { path: '/account_activations/:token/edit', component: AwaitingActivation }
  ],
  mode: "history"
});

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app')
