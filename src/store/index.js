import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({

  state: {
    count: 0,
    globalUser: null,
    id: localStorage.getItem('id') || '',
    selectedId: localStorage.getItem('selectedId') || '',
    selectedIndex: [],
    sort: 'newest',
    route: "users/followed_bets",
    page: 1
  },

  mutations: {
    incrementCounter (state, payload) {
      state.count += payload
    },
    saveGlobalUser (state, payload) {
      state.globalUser = payload.data.user
      state.id = payload.data.user['id']
      localStorage.setItem('client', payload.headers['client'])
      localStorage.setItem('access-token', payload.headers['access-token'])
      localStorage.setItem('uid', payload.headers['uid'])
      localStorage.setItem('id', state.id)
    },
    destroyGlobalUser (state) {
      if (state.id != '') {
        state.globalUser = ''
        state.id = ''
        localStorage.setItem('client', '')
        localStorage.setItem('access-token', '')
        localStorage.setItem('uid', '')
        localStorage.setItem('id', '')
      }
    },
    saveSelectedId (state, payload) {
      state.selectedId = payload
      localStorage.setItem('selectedId', state.selectedId)
    },
    saveSelectedIndex (state, payload) {
      console.log("payload in arrived")
      state.selectedIndex.push(...payload)
    },
    saveBet (state, payload) {
      let index = state.selectedIndex.findIndex(item => item.id === payload.id)
      let merged = {...state.selectedIndex[index], ...payload};
      // console.log("merging action - wow - so good, and no bugs!")
      // console.log(JSON.stringify(state.selectedIndex[index]))
      // console.log(JSON.stringify(payload))
      // console.log(JSON.stringify(merged))
      state.selectedIndex.splice(index, 1, merged)
    },
    deleteBet (state, payload) {
      let index = state.selectedIndex.findIndex(item => item.id === payload.id)
      state.selectedIndex.splice(index, 1)
    },
    deleteSelectedIndex (state) {
      state.selectedIndex = []
    },
    setSort (state, payload) {
      state.sort = payload
    },
    setRoute (state, payload) {
      state.route = payload
    },
    setPage (state, payload) {
      state.page = payload
    },
  },
  actions: {
    incrementAction ({commit}, payload) {
      commit('incrementCounter', payload)
    },
    saveGlobalUserAction ({commit}, payload) {
        commit('saveGlobalUser', payload)
    },
    destroyGlobalUserAction ({commit}) {
      commit('destroyGlobalUser')
    },
    saveSelectedIdAction ({commit}, payload) {
      commit('saveSelectedId', payload)
    },
    saveSelectedIndexAction ({commit}, payload) {
      console.log("payload in transit")
      commit('saveSelectedIndex', payload)
    },
    saveBetAction ({commit}, payload) {
      commit('saveBet', payload)
    },
    deleteBetAction ({commit}, payload) {
      commit('deleteBet', payload)
    },
    deleteSelectedIndexAction ({commit}) {
      commit('deleteSelectedIndex')
    },
    setSortAction ({commit}, payload) {
      commit('setSort', payload)
    },
    setRouteAction ({commit}, payload) {
      commit('setRoute', payload)
    },
    setPageAction ({commit}, payload) {
      commit('setPage', payload)
    }
  },

  getters: {
    counter (state) {
      return state.count
    },
    globalUser (state) {
        return state.globalUser       
    },
    id (state) {
      return state.id
    },
    selectedId(state) {
      return state.selectedId
    },
    selectedIndex(state) {
      return state.selectedIndex
    },
    sort(state) {
      return state.sort
    },
    route(state) {
      return state.route
    },
    page(state) {
      return state.page
    }
  }
})