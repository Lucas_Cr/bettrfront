const { VueLoaderPlugin } = require("vue-loader");

// const htmlWebpackPlugin = require("html-webpack-plugin");
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const { CleanWebpackPlugin } = require("clean-webpack-plugin");
// const autoprefixer = require("autoprefixer");
const path = require("path");

module.exports = {
  entry: {
    main: "./src/main.js",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.vue$/,
        loader: "vue-loader",
      },
      // {
      //   test: /\.(eot|ttf|woff|woff2)(\?\S*)?$/,
      //   loader: "file-loader",
      //   options: {
      //     name: "[name][contenthash:8].[ext]",
      //   },
      // },
      // {
      //   test: /\.(png|jpe?g|gif|webm|mp4|svg)$/,
      //   loader: "file-loader",
      //   options: {
      //     name: "[name][contenthash:8].[ext]",
      //     outputPath: "assets/img",
      //     esModule: false,
      //   },
      // },
      // {
      //   test: /\.s?css$/,
      //   use: [
      //     "style-loader",
      //     MiniCssExtractPlugin.loader,
      //     "css-loader",
      //     {
      //       loader: "postcss-loader",
      //       options: {
      //         plugins: () => [autoprefixer()],
      //       },
      //     },
      //     "sass-loader",
      //   ],
      // },
      {
        test: /\.s(c|a)ss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            // Requires sass-loader@^7.0.0
            options: {
              implementation: require('sass'),
              indentedSyntax: true // optional
            },
            // Requires sass-loader@^8.0.0
            options: {
              implementation: require('sass'),
              sassOptions: {
                indentedSyntax: true // optional
              },
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
  ],
  resolve: {
    alias: {
      vue$: "vue/dist/vue.runtime.esm.js",
    },
    extensions: ["*", ".js", ".vue", ".json"],
  },
};